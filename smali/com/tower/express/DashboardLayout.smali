.class public Lcom/tower/express/DashboardLayout;
.super Landroid/view/ViewGroup;
.source "DashboardLayout.java"


# static fields
.field private static final UNEVEN_GRID_PENALTY_MULTIPLIER:I = 0xa


# instance fields
.field private mMaxChildHeight:I

.field private mMaxChildWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    iput v1, p0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    .line 16
    iput v1, p0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, p2, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    iput v0, p0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    .line 16
    iput v0, p0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    iput v0, p0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    .line 16
    iput v0, p0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    .line 28
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 17

    .prologue
    .line 78
    sub-int v8, p4, p2

    .line 79
    sub-int v9, p5, p3

    .line 81
    invoke-virtual/range {p0 .. p0}, Lcom/tower/express/DashboardLayout;->getChildCount()I

    move-result v10

    .line 84
    const/4 v2, 0x0

    .line 85
    const/4 v1, 0x0

    move/from16 v16, v1

    move v1, v2

    move/from16 v2, v16

    :goto_0
    if-lt v2, v10, :cond_1

    .line 93
    if-nez v1, :cond_3

    .line 171
    :cond_0
    return-void

    .line 86
    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/tower/express/DashboardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 87
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    .line 85
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 90
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 99
    :cond_3
    const v3, 0x7fffffff

    .line 106
    const/4 v2, 0x1

    .line 110
    :goto_2
    add-int/lit8 v4, v1, -0x1

    div-int/2addr v4, v2

    add-int/lit8 v5, v4, 0x1

    .line 112
    move-object/from16 v0, p0

    iget v4, v0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    mul-int/2addr v4, v2

    sub-int v4, v8, v4

    add-int/lit8 v6, v2, 0x1

    div-int v7, v4, v6

    .line 113
    move-object/from16 v0, p0

    iget v4, v0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    mul-int/2addr v4, v5

    sub-int v4, v9, v4

    add-int/lit8 v6, v5, 0x1

    div-int v6, v4, v6

    .line 115
    sub-int v4, v6, v7

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 116
    mul-int v11, v5, v2

    if-eq v11, v1, :cond_4

    .line 117
    mul-int/lit8 v4, v4, 0xa

    .line 120
    :cond_4
    if-ge v4, v3, :cond_5

    .line 126
    const/4 v3, 0x1

    if-ne v5, v3, :cond_6

    move v1, v5

    move v3, v6

    move v4, v7

    .line 144
    :goto_3
    const/4 v5, 0x0

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 145
    const/4 v4, 0x0

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 148
    add-int/lit8 v3, v2, 0x1

    mul-int/2addr v3, v7

    sub-int v3, v8, v3

    div-int v8, v3, v2

    .line 149
    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v11

    sub-int v3, v9, v3

    div-int v9, v3, v1

    .line 153
    const/4 v4, 0x0

    .line 154
    const/4 v3, 0x0

    move v6, v3

    :goto_4
    if-ge v6, v10, :cond_0

    .line 155
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/tower/express/DashboardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 156
    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v5, 0x8

    if-ne v3, v5, :cond_7

    move v3, v4

    .line 154
    :goto_5
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    goto :goto_4

    .line 131
    :cond_5
    add-int/lit8 v2, v2, -0x1

    .line 132
    add-int/lit8 v1, v1, -0x1

    div-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 133
    move-object/from16 v0, p0

    iget v3, v0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    mul-int/2addr v3, v2

    sub-int v3, v8, v3

    add-int/lit8 v4, v2, 0x1

    div-int v4, v3, v4

    .line 134
    move-object/from16 v0, p0

    iget v3, v0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    mul-int/2addr v3, v1

    sub-int v3, v9, v3

    add-int/lit8 v5, v1, 0x1

    div-int/2addr v3, v5

    goto :goto_3

    .line 138
    :cond_6
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    .line 109
    goto :goto_2

    .line 160
    :cond_7
    div-int v13, v4, v2

    .line 161
    rem-int v3, v4, v2

    .line 163
    add-int/lit8 v5, v3, 0x1

    mul-int/2addr v5, v7

    mul-int v14, v8, v3

    add-int/2addr v14, v5

    .line 164
    add-int/lit8 v5, v13, 0x1

    mul-int/2addr v5, v11

    mul-int v15, v9, v13

    add-int/2addr v15, v5

    .line 167
    if-nez v7, :cond_8

    add-int/lit8 v5, v2, -0x1

    if-ne v3, v5, :cond_8

    move/from16 v5, p4

    .line 168
    :goto_6
    if-nez v11, :cond_9

    add-int/lit8 v3, v1, -0x1

    if-ne v13, v3, :cond_9

    move/from16 v3, p5

    .line 166
    :goto_7
    invoke-virtual {v12, v14, v15, v5, v3}, Landroid/view/View;->layout(IIII)V

    .line 169
    add-int/lit8 v3, v4, 0x1

    goto :goto_5

    .line 167
    :cond_8
    add-int v3, v14, v8

    move v5, v3

    goto :goto_6

    .line 168
    :cond_9
    add-int v3, v15, v9

    goto :goto_7
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/16 v8, 0x8

    const/high16 v3, -0x80000000

    const/4 v0, 0x0

    .line 32
    iput v0, p0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    .line 33
    iput v0, p0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    .line 38
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 37
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 40
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 39
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 42
    invoke-virtual {p0}, Lcom/tower/express/DashboardLayout;->getChildCount()I

    move-result v4

    move v1, v0

    .line 43
    :goto_0
    if-lt v1, v4, :cond_0

    .line 58
    iget v1, p0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    .line 57
    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 60
    iget v2, p0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    .line 59
    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 62
    :goto_1
    if-lt v0, v4, :cond_2

    .line 72
    iget v0, p0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    invoke-static {v0, p1}, Lcom/tower/express/DashboardLayout;->resolveSize(II)I

    move-result v0

    .line 73
    iget v1, p0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    invoke-static {v1, p2}, Lcom/tower/express/DashboardLayout;->resolveSize(II)I

    move-result v1

    .line 71
    invoke-virtual {p0, v0, v1}, Lcom/tower/express/DashboardLayout;->setMeasuredDimension(II)V

    .line 74
    return-void

    .line 44
    :cond_0
    invoke-virtual {p0, v1}, Lcom/tower/express/DashboardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 45
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-ne v6, v8, :cond_1

    .line 43
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {v5, v2, v3}, Landroid/view/View;->measure(II)V

    .line 51
    iget v6, p0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, p0, Lcom/tower/express/DashboardLayout;->mMaxChildWidth:I

    .line 52
    iget v6, p0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/tower/express/DashboardLayout;->mMaxChildHeight:I

    goto :goto_2

    .line 63
    :cond_2
    invoke-virtual {p0, v0}, Lcom/tower/express/DashboardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 64
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-ne v5, v8, :cond_3

    .line 62
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 68
    :cond_3
    invoke-virtual {v3, v1, v2}, Landroid/view/View;->measure(II)V

    goto :goto_3
.end method
